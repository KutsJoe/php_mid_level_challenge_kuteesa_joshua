<?php
  $page_title = 'Question 1';
  include 'head.php';
?>

<?php

#Generate the array of numbers
$numbers = range(0,100);

#Create an array to store our fibonacci numbers with 0 and 1 to start of our fibinacci sequence
$fibonacci = [];

foreach($numbers as $number){
  if($number == 0){
    array_push($fibonacci,$number);
  }else if($number == 1){
    array_push($fibonacci,$number);
  }else{
    #Calculate the next fibinacci using the first two in the array
    $next = ($fibonacci[count($fibonacci) -1]) + ($fibonacci[count($fibonacci) - 2]);
    #Make sure we don't exceed 100
    if($next < 100){
      array_push($fibonacci,$next);
    }
  }
}

?>


<div class="container mt-4">
  <div class="row align-items-center">
    <div class="col"></div>
    <div class="col">
      <ul class="list-group">
        <?php
          $count = 0;
          foreach($fibonacci as $number):
            #only diplay begining with the 7th
            if($count >= 7):
        ?>
          <li class="list-group-item"><?= $number ?></li>
        <?php
            endif;
           $count++;
          endforeach;
        ?>
      </ul>
    </div>
    <div class="col"></div>
  </div>

  <div class="row justify-content-center mt-4">
    <div class="col-sm-4">
      <a href="index.php" class="btn btn-sm btn-dark"> < Back </a>
    </div>
  </div>
</div>


<?php 

include 'bottom.php';

?>