<?php

$page_title = 'Question 3 parta';
include 'head.php';

function returnUniqueWords($filename) {
  #Read the contents of the text file
  $file_content = file_get_contents($filename);
  
  #Split the contents into an array of words
  $all_words = preg_split('/\s+/', $file_content, -1, PREG_SPLIT_NO_EMPTY);
  
  #Remove duplicates from the array
  $unique_words = array_unique($all_words);
  
  #Reset array keys to start from 0
  $unique_words = array_values($unique_words);
  
  return $unique_words;
}

function returnPuncuationMarks($file) {
  #Read the contents of the txt file
  $file_content = file_get_contents($file);
  
  #Use a regular expression to extract punctuation marks
  preg_match_all("/[\p{P}]/u", $file_content, $matches);
  
  #Remove duplicates
  $all_marks = array_unique($matches[0]);
  
  return $all_marks;
}

$filename = "test-file.txt";
$uniqueWords = returnUniqueWords($filename);

$allPuncuationMarks = returnPuncuationMarks($filename);

?>

<div class="row justify-content-center mt-4">
  <div class="col-sm-4">
    <a href="index.php" class="btn btn-sm btn-dark"> < Back </a>
  </div>
</div>

<div class="container mt-4">
  <div class="row align-items-center">
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          Unique Words
        </div>
        <div class="card-body">
          <?php
            foreach($uniqueWords as $word):
          ?>
          <p class="card-text"><?= $word ?></p>
          <?php
            endforeach;
          ?>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          All Punctuation
        </div>
        <div class="card-body">
          <?php
            foreach($allPuncuationMarks as $mark):
          ?>
          <p class="card-text"><?= $mark ?></p>
          <?php
            endforeach;
          ?>
        </div>
      </div>
    </div>
  </div>
</div>

<?php 

include 'bottom.php';

?>