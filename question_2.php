<?php
  $page_title = 'Question 1';
  include 'head.php';

  $string = "This is a paragraph and it has to find 256781123456, testemail@gmail.com and https://kanzucode.com/";

  function findEmailUsingRegex(){
    global $string;

    $pattern = '/[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}/';

    preg_match($pattern, $string, $matches);
  
    $email = $matches[0];

    return $email;

  }

  function findNumberUsingRegex(){
    global $string;

    $pattern = '/\b' . preg_quote('256', '/') . '\d+\b/';

    preg_match_all($pattern, $string, $numbers);

    return $numbers[0][0];
    
  }


  function findUrlUsingRegex(){
    global $string;

    $pattern = '/\bhttps:\/\/\S+\b/';

    preg_match_all($pattern, $string, $urls);

    return $urls[0][0];
    
  }


  function findUsing($symbol,$string){
    #use a unique symbol to indetify the desired string
    $symbolPosition = strpos($string, $symbol);

    #find the space before desired string
    $spaceInfront = strrpos(substr($string, 0, $symbolPosition), " ");

    #Find space after the string
    $spaceAfter = strpos(substr($string, $symbolPosition), " ");

    #Find the start and end of the string
    $start = ($spaceInfront !== false) ? $spaceInfront + 1 : 0;
    $end = ($spaceAfter !== false) ? $symbolPosition + $spaceAfter : strlen($string);

    #Extract the string
    return substr($string, $start, $end - $start);
  }

  function findEmail(){
    global $string;

    #use @ as unique email identifier
    $email = findUsing('@',$string);

    return $email;

  }

  function findNumber(){
    global $string;

    #use 256 to find the number 
    $number = findUsing('256',$string);

    #remove the comma close to the number
    return str_replace(',','',$number);

  }

  function findUrl(){
    global $string;

    #use :// as a unique identifier
    $number = findUsing('://',$string);

    return $number;

  }

;

?>

<div class="container mt-4">
  <div class="row align-items-center">
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          Using Regex
        </div>
        <div class="card-body">
          <h5 class="card-title">Email</h5>
          <p class="card-text"><?= findEmailUsingRegex(); ?></p>
          <h5 class="card-title">Number</h5>
          <p class="card-text"><?= findNumberUsingRegex(); ?></p>
          <h5 class="card-title">Url</h5>
          <p class="card-text"><?= findUrlUsingRegex(); ?></p>
        </div>
      </div>
    </div>
  </div>

  <div class="row align-items-center mt-4">
    <div class="col-md-4">
      <div class="card">
        <div class="card-header">
          Without Regex
        </div>
        <div class="card-body">
          <h5 class="card-title">Email</h5>
          <p class="card-text"><?= findEmail(); ?></p>
          <h5 class="card-title">Number</h5>
          <p class="card-text"><?= findNumber(); ?></p>
          <h5 class="card-title">Url</h5>
          <p class="card-text"><?= findUrl(); ?></p>
        </div>
      </div>
    </div>
  </div>

  
  <div class="row justify-content-center mt-4">
    <div class="col-sm-4">
      <a href="index.php" class="btn btn-sm btn-dark"> < Back </a>
    </div>
  </div>

</div>
<?php 

include 'bottom.php';

?>