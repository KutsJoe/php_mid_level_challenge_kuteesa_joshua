<?php

namespace App\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Auth;

class Milestone extends Model
{
    use HasFactory;

    protected $fillable = [
        'title','project'
    ];

    public function creator(): BelongsTo {
        return $this->belongTo(User::class, 'user_id');
    }

    public function project(): BelongsTo {
        return $this->BelongsTo(Project::class, 'project');
    }
    
    // protected static function booted(): void {
    //     static::addGlobalScope('creator', function(Builder $builder){
    //         $builder->where('user_id', Auth::id());
    //     });
    // }
}
