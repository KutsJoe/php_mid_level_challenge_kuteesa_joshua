<?php

namespace App\Models;

use Illuminate\Contracts\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Auth;

class Project extends Model
{
    use HasFactory;

    protected $fillable = [
        'name','status','developer_id','pm_id'
    ];

    public function creator(): BelongsTo {
        return $this->belongTo(User::class, 'developer_id');
    }

    public function milestones(): HasMany{
        return $this->hasMany(Milestone::class, 'project');
    }

    // protected static function booted(): void {
    //     static::addGlobalScope('creator', function(Builder $builder){
    //         $builder->where('developer_id', Auth::id());
    //     });
    // }
}
