<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Project;
use App\Http\Requests\StoreProjectRequest;
use App\Http\Requests\UpdateProjectRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\ProjectCollection;
use App\Http\Resources\ProjectResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::user();

        #retrun only project assigned if an engineer is logged in
        if($user->role == 'engineer')
        {
            $projects = Project::where('developer_id', $user->id)->get();
        }else{
            $projects = Project::all();
        }

        return new ProjectCollection($projects);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProjectRequest $request)
    {
        $validated = $request->validated();

        #Get logged in user and if they are developer don't allow saving of done status
        $user = Auth::user();

        if($user->role == 'engineer' && $request->status == 'completed'){

            return response()->json([
                'message' => 'Engineer not allowed to set status as completed',

            ], 401);
        }

        $project = $user->projects()->create($validated);

        return new ProjectResource($project);
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request,Project $project)
    {
        return new ProjectResource($project);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Project $project)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProjectRequest $request, Project $project)
    {
        $validated = $request->validated();


        #Get logged in user and if they are developer don't allow saving of done status
        $user = Auth::user();

        if($user->role == 'engineer' && $request->status == 'completed'){

            return response()->json([
                'message' => 'Engineer not allowed to set status as completed',

            ], 401);
        }

        #If PM is making a request populate pm field and allow only pm to change assigned developer
        #Handle change of developer id
        if($request->developer_id){
            #Try to get the user and see if they exist
            $developer = User::findOrFail($request->developer_id);

            if($developer){

                $project->update(['developer_id' => $request->developer_id]);
                
            }else{
                
                return response()->json([
                    'message' => 'Engineer does not exist',
                ], 401);
            }

        }

        if($user->role == 'pm'){
            #Add pm field too
            $project->update(['pm_id' => $user->id]);
        }
        
        if($request->name){
            $project->update(['name' => $request->name]);
        }

        if($request->status){
            $project->update(['status' => $request->status]);
        }

        return new ProjectResource($project);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Project $project)
    {
        $project->delete();

        return response()->noContent();
    }

}
