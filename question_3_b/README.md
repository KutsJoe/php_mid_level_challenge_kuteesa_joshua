## About Question 3 section b

I built this using [Laravel 10](https://laravel.com/docs/10.x/installation) and composer so, please make sure you have pre-requisites mentioned for a composer laravel installation.

## Running the project
- cd into question_3_b folder and create the env file by copying .env.example and renaming it to env
- Replace these details with your database connection details
  ```bash
  DB_CONNECTION=mysql
  DB_HOST=127.0.0.1
  DB_PORT=3306
  DB_DATABASE=
  DB_USERNAME=
  DB_PASSWORD=
  ```
- open a terminal either in your text editor or your operating system, make sure you are in the same directory as the project and run `php artisan migrate` you can add the --seed option to run the faker library that generates some 10 projects for you to start with i.e `php artisan migrate --seed`
- run `php artisan serve` and type http://localhost:8000/ to view the project

### Examples

- Start of by creating a new user using the http://localhost:8000/api/register endpoint and set role to either pm or engineer.
Below is sample raw json payload to send to the api.

```json
{
    "name": "User",
    "email": "user@mail.com",
    "role": "engineer",
    "password": "password",
    "password_confirmation": "password"
}
```
- Use the returned token to make all api calls
- You need to create a project first and attach a milestone to it this is a sample request to create a milestone

```json
{
  "title": "First Milestone",
  "project": 1 //Project id
}
```

## NB at the root of this repository is a postman collection with sample API requests that can be used to test the API




