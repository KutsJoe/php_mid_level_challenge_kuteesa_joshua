<?php
  $page_title = 'All Questions';
  include 'head.php';
?>
<div class="container mt-4">
  <div class="row align-items-center">
    <div class="col"></div>
    <div class="col">
      <div class="list-group">
        <a href="question_1.php" class="list-group-item list-group-item-action" >
          Question 1
        </a>
        <a href="question_2.php" class="list-group-item list-group-item-action" >
          Question 2
        </a>
        <a href="question_3_a.php" class="list-group-item list-group-item-action" >
          Question 3 a
        </a>
        <a href="/question_3_b/" class="list-group-item list-group-item-action" >
          Question 3 b
        </a>
        <a href="question_3_c.php" class="list-group-item list-group-item-action" >
          Question 3 c
        </a>
      </div>
    </div>
    <div class="col"></div>
  </div>
</div>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
-->

<?php 

include 'bottom.php';

?>


